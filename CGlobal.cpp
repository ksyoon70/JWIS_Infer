#include "pch.h"
#include "CGlobal.h"


/// 이 프로그램의 실행 파일 디렉터리 PATH.
TCHAR CGlobal::Program_PATH[] = { 0, };
TCHAR CGlobal::Program_Name[] = { 0, };           // 이 프로그램의 실행 파일 이름
TCHAR CGlobal::PROGRAM_INI_FULLPATH[] = { 0,0 };   // 이 프로그램에서 사용할 INI파일에 대한 full path를 가지고 있는 변수.
TCHAR CGlobal::Start_PATH[] = { 0 };
TCHAR CGlobal::Model_PATH[] = { 0 };				//모델 Path


CGlobal::CGlobal()
{

}

/**
* @author 윤경섭
* @file CGlobal.cpp
* @fn  CGlobal::~CGlobal()
* @brief CGlobal의 소멸자
* @remark 내용없음
* @return 리턴값 없음
* @date 20/02/19
*/
CGlobal::~CGlobal()
{
}

/**
* @author 윤경섭
* @file CGlobal.cpp
* @fn  void CGlobal::GLB_Read_INI_COMMON(void)
* @param void 없음
* @brief INI 값을 읽는다
* @return 리턴값 없음
* @date 20/02/19
*/
void CGlobal::GLB_Read_INI_COMMON(void)
{
	if (_tcslen(CGlobal::PROGRAM_INI_FULLPATH) > 0)
	{
		GetPrivateProfileString(_T("SYSTEM"), _T("START_PATH"), _T(""), CGlobal::Start_PATH, sizeof(CGlobal::Start_PATH), CGlobal::PROGRAM_INI_FULLPATH);
	}
}

CGlobal GLB_Global;