﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// JWISInfer.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_JWIS_INFER_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_FILE_LIST                   1000
#define IDC_AUTO_BUTTON                 1001
#define IDC_FOLDERS_TREE                1002
#define IDC_SAVE_BUTTON                 1003
#define IDC_PC_VIEW_STATIC              1004
#define IDC_CLASSID_EDIT                1005
#define IDC_EDIT2                       1006
#define IDC_INFER_SPEED_EDIT            1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
