#pragma once
#include <iostream>
#include "tensorflow/c/c_api.h"
#include <codecvt>
#include <fstream>
#include <vector>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/core/types.hpp>
#include<opencv2/imgproc.hpp>
#include <opencv2/dnn/dnn.hpp>

extern void DeallocateBuffer(void* data, size_t);

class Network
{
public:
	void LoadGraph(std::string modelPath);
	void Run(cv::Mat  image_record, PDWORD pElapseTime, PUINT pClass);
	static void Deallocator(void* data, size_t length, void* arg);
	Network();
	~Network();
private:
	TF_Session* session;
	TF_Graph* graph;
};

