﻿
// JWIS_InferDlg.h: 헤더 파일
//

#pragma once
#include "ProjectListCtrl.h"
#include "ProjFilesArray.h"
#include "MFCShellTreeCtrlEx.h"

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/core/types.hpp>
#include<opencv2/imgproc.hpp>

#include "Network.h"

// CJWISInferDlg 대화 상자
class CJWISInferDlg : public CDialogEx
{
// 생성입니다.
public:
	CJWISInferDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.
	virtual ~CJWISInferDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_JWIS_INFER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	CMFCShellTreeCtrlEx m_cTreeCtrl;
	CProjectListCtrl m_File_List;
	CProjFilesArray m_cProjFileMap;
	CString m_cRootFolder;
	cv::Mat m_matImage;
	BITMAPINFO* m_pBitmapInfo;

	Network *pSsd_network;

	UINT m_classValue;
	DWORD m_InferSpeedValue;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	void DrawImage();
	void CreateBitmapInfo(int w, int h, int bpp);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTvnSelchangedFoldersTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedSaveButton();
	afx_msg void OnNMClickFileList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedAutoButton();
};
