#pragma once
class CGlobal
{
public :
		CGlobal();
		virtual ~CGlobal();
		static TCHAR Program_PATH[MAX_PATH];           // 이 프로그램의 실행 파일 디렉터리 PATH.
		static TCHAR PROGRAM_INI_FULLPATH[MAX_PATH];   // 이 프로그램에서 사용할 INI파일에 대한 full path를 가지고 있는 변수.
		static TCHAR Program_Name[MAX_PATH];           // 이 프로그램의 실행 파일 이름
		static TCHAR Start_PATH[MAX_PATH];			   // 디렉토리 시작 PATH
		static TCHAR Model_PATH[MAX_PATH];			   // 모델 Path

public:
	void GLB_Read_INI_COMMON(void);
	
};

extern CGlobal GLB_Global;