﻿
// JWIS_InferDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "JWIS_Infer.h"
#include "JWIS_InferDlg.h"
#include "afxdialogex.h"
#include "CGlobal.h"

#include <iostream>
#include "tensorflow/c/c_api.h"
#include <codecvt>
#include <fstream>
#include <vector>
#include <mmsystem.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CJWISInferDlg 대화 상자



CJWISInferDlg::CJWISInferDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_JWIS_INFER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_classValue = 0;
	m_InferSpeedValue = 0;
	pSsd_network = NULL;
}


CJWISInferDlg::~CJWISInferDlg()
{
	if (pSsd_network != NULL)
		delete pSsd_network;
	pSsd_network = NULL;

	m_matImage = NULL;
}
void CJWISInferDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILE_LIST, m_File_List);
	DDX_Control(pDX, IDC_FOLDERS_TREE, m_cTreeCtrl);
	DDX_Text(pDX, IDC_CLASSID_EDIT, m_classValue);
	DDX_Text(pDX, IDC_INFER_SPEED_EDIT, m_InferSpeedValue);
}

BEGIN_MESSAGE_MAP(CJWISInferDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TVN_SELCHANGED, IDC_FOLDERS_TREE, &CJWISInferDlg::OnTvnSelchangedFoldersTree)
	ON_BN_CLICKED(IDC_SAVE_BUTTON, &CJWISInferDlg::OnBnClickedSaveButton)
	ON_NOTIFY(NM_CLICK, IDC_FILE_LIST, &CJWISInferDlg::OnNMClickFileList)
	ON_BN_CLICKED(IDC_AUTO_BUTTON, &CJWISInferDlg::OnBnClickedAutoButton)
END_MESSAGE_MAP()


// CJWISInferDlg 메시지 처리기

BOOL CJWISInferDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_File_List.SetItemTypes(SHCONTF_NONFOLDERS);
	// Passing project files map pointer to both shell list controls
	m_File_List.SetProjectFiles(&m_cProjFileMap);

	m_File_List.SetExtendedStyle(m_File_List.GetExtendedStyle() | LVS_EX_FULLROWSELECT);

	// Setting list control filters
	m_File_List.SetFilter(LISTFILTER_REMAINING);

	HTREEITEM hParentItem = m_cTreeCtrl.GetRootItem();
	//HTREEITEM hParentItem;
		
	m_cTreeCtrl.SelectItem(hParentItem);
	
	m_cTreeCtrl.Expand(hParentItem, TVE_EXPAND);

	if (_tcslen(CGlobal::Start_PATH) > 0)
	{
		m_cTreeCtrl.SelectPath(CGlobal::Start_PATH);		
	}

	HTREEITEM hItem = m_cTreeCtrl.GetSelectedItem();
	CString cFolderPath;
	if (m_cTreeCtrl.GetItemPath(cFolderPath, hItem))
	{
		// Retrieving an existing (or adding a new: bAddIfNotFound = TRUE) project files array
		CStringArray* pFilesArr = m_cProjFileMap.GetFiles(cFolderPath, TRUE);
		// and using the resultant pointer as the selected tree item data
		m_cTreeCtrl.SetItemDataEx(hItem, (DWORD_PTR)pFilesArr);
		// Refreshing both list controls contents
		m_File_List.DisplayFolder(cFolderPath);
	}


	std::string modelPath = (CT2CA)CGlobal::Model_PATH;

	pSsd_network = new Network();

	pSsd_network->LoadGraph(modelPath);
	
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CJWISInferDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CJWISInferDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CJWISInferDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CJWISInferDlg::OnTvnSelchangedFoldersTree(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	HTREEITEM hItem = m_cTreeCtrl.GetSelectedItem();
	CString cFolderPath;
	if (m_cTreeCtrl.GetItemPath(cFolderPath, hItem))
	{
		// Retrieving an existing (or adding a new: bAddIfNotFound = TRUE) project files array
		CStringArray* pFilesArr = m_cProjFileMap.GetFiles(cFolderPath, TRUE);
		// and using the resultant pointer as the selected tree item data
		m_cTreeCtrl.SetItemDataEx(hItem, (DWORD_PTR)pFilesArr);
		// Refreshing both list controls contents
		m_File_List.DisplayFolder(cFolderPath);
	}

	*pResult = 0;
}


void CJWISInferDlg::OnBnClickedSaveButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString cFolderPath;
	HTREEITEM hItem = m_cTreeCtrl.GetSelectedItem();
	m_cTreeCtrl.GetItemPath(cFolderPath, hItem);

	_stprintf_s(CGlobal::Start_PATH, _countof(CGlobal::Start_PATH), _T("%s"), cFolderPath);

	WritePrivateProfileString(_T("SYSTEM"), _T("START_PATH"), CGlobal::Start_PATH, CGlobal::PROGRAM_INI_FULLPATH);
}


void CJWISInferDlg::OnNMClickFileList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strPath;

	m_File_List.GetItemPath(strPath, pNMItemActivate->iItem);
	cv::String  path;
	path = (CT2CA)strPath;
	m_matImage = cv::imread(path, cv::IMREAD_UNCHANGED);

	m_matImage.convertTo(m_matImage, CV_8UC3);

	DWORD elapseTime;
	UINT classId;
	pSsd_network->Run(m_matImage, &elapseTime, &classId);

	m_classValue = classId;
	m_InferSpeedValue = elapseTime;
	
	CreateBitmapInfo(m_matImage.cols, m_matImage.rows, m_matImage.channels() * 8);

	DrawImage();

	UpdateData(FALSE);
	
	*pResult = 0;
}
void CJWISInferDlg::DrawImage()
{
	CClientDC dc(GetDlgItem(IDC_PC_VIEW_STATIC));

	CRect rect;
	GetDlgItem(IDC_PC_VIEW_STATIC)->GetClientRect(&rect);

	SetStretchBltMode(dc.GetSafeHdc(), COLORONCOLOR);
	StretchDIBits(dc.GetSafeHdc(), 0, 0, rect.Width(), rect.Height(), 0, 0, m_matImage.cols, m_matImage.rows, m_matImage.data, m_pBitmapInfo, DIB_RGB_COLORS, SRCCOPY);
}

void CJWISInferDlg::CreateBitmapInfo(int w, int h, int bpp)
{

	if (m_pBitmapInfo != NULL)
	{
		delete m_pBitmapInfo;
		m_pBitmapInfo = NULL;
	}

	if (bpp == 8)
		m_pBitmapInfo = (BITMAPINFO*) new BYTE[sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD)];
	else // 24 or 32bit
		m_pBitmapInfo = (BITMAPINFO*) new BYTE[sizeof(BITMAPINFO)];

	m_pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_pBitmapInfo->bmiHeader.biPlanes = 1;
	m_pBitmapInfo->bmiHeader.biBitCount = bpp;
	m_pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	m_pBitmapInfo->bmiHeader.biSizeImage = 0;
	m_pBitmapInfo->bmiHeader.biXPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biClrUsed = 0;
	m_pBitmapInfo->bmiHeader.biClrImportant = 0;

	if (bpp == 8)
	{
		for (int i = 0; i < 256; i++)
		{
			m_pBitmapInfo->bmiColors[i].rgbBlue = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbGreen = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbRed = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbReserved = 0;
		}
	}

	m_pBitmapInfo->bmiHeader.biWidth = w;
	m_pBitmapInfo->bmiHeader.biHeight = -h;
}

void CJWISInferDlg::OnBnClickedAutoButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
